import dbConnect from '$lib/db.js';
import Blogs from '$lib/models/blogs';

export async function get(request) {
	// const id = request.query;
	await dbConnect();
	// const firstblog = new Blogs({
	// 	title: 'first Post',
	// 	description: 'first description',
	// 	excerpt: 'first excerpt'
	// });
	// firstblog.save();

	const blogs = await Blogs.find();
	// const blogs = [
	// 	{ id: 1, title: 'some title' },
	// 	{ id: 2, title: 'some title 2' },
	// 	{ id: 3, title: 'some title 3' },
	// 	{ id: 4, title: 'some title 4' },
	// 	{ id: 5, title: 'some title 5' }
	// ];
	return {
		status: 200,
		body: { blogs }
	};
}

export async function post(request) {}

export async function put(request) {}

export async function del(request) {}
